/* @formatter:off
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
@formatter:on */

package seminar1.task1;

import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.Arrays;

public class bookstore {

    public static enum BoxPrice {
        FULL_PRICE(1, 5, 8), BULK_DISCOUNT(6, Integer.MAX_VALUE, 5);

        private final int minBooks;
        private final int maxBooks;
        private final int price;

        private BoxPrice(int minBooks, int maxBooks, int price) {
            this.minBooks = minBooks;
            this.maxBooks = maxBooks;
            this.price = price;
        }

        public static int price(int boxes) {
            return Arrays.stream(values()).filter(r -> boxes >= r.minBooks && boxes <= r.maxBooks)
                    .findFirst().orElse(null).price * boxes;
        }
    }

    final static int BOOKS_PER_BOOK = 5;

    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);
        Integer books;

        while (true) {
            System.out.print("\nHow many books do you have for shipping? ");
            if (scanner.hasNextInt()) {
                books = scanner.nextInt();
                if (books > 0)
                    break;
                else
                    System.out.println("Please insert a number of books greater than 0.");
            } else {
                System.out.println("Please insert a number.");
                scanner.next();
            }
        }
        scanner.close();

        int[] boxes = calculateNumberOfBoxes(books);
        int price = BoxPrice.price(boxes.length);

        System.out.println("\nNumber of books: " + books);
        System.out.println("Number of boxes: " + boxes.length);

        IntStream.range(1, boxes.length + 1).forEach(
                i -> System.out.println("Box " + i + " contains " + boxes[--i] + " books."));

        System.out.println("The shipping price will be: " + price + " SEK");

    }

    public static int[] calculateNumberOfBoxes(int books) {
        // Force Java to perform a division between doubles
        // If not Math.ceil does not round up properly
        int numberOfBoxes = (int) Math.ceil((double) books / BOOKS_PER_BOOK);
        int booksLastBox = (books % BOOKS_PER_BOOK);
        int boxes[] = new int[numberOfBoxes];
        Arrays.fill(boxes, BOOKS_PER_BOOK);
        if (booksLastBox != 0)
            boxes[boxes.length - 1] = booksLastBox;
        return boxes;
    }

}
