/* @formatter:off
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
@formatter:on */

package seminar1.task3;

import java.util.Scanner;

public class travelTime {

    public static void main(String args[]) {
        System.out.println("This program will calculate the remaining travel time");
        Scanner scanner = new Scanner(System.in);
        double averageSpeed;
        double remainingMilieage;
        // Let the user inserts negative values and eventually calculate a negative time.
        // It is a bit weird but there aren't side effects, no exceptions are thrown
        // and it can, somehow, be useful.
        while (true) {
            System.out.print("\nEnter the average speed <km/h>: ");
            if (scanner.hasNextDouble()) {
                averageSpeed = scanner.nextDouble();
                break;
            }
            System.out.println("Please insert a number");
            scanner.next();
        }
        while (true) {
            System.out.print("\nEnter the remaining mileage <mile>: ");
            if (scanner.hasNextDouble()) {
                remainingMilieage = scanner.nextDouble();
                break;
            }
            System.out.println("Please insert a number");
            scanner.next();
        }
        scanner.close();
        int hours = (int) (remainingMilieage * 10 / averageSpeed);
        int minutes = (int) ((remainingMilieage * 10 / averageSpeed - hours) * 60);
        System.out.println("\nYour remaining travel time is: " + hours + " hour(s) and " + minutes
                + " minute(s).\n");
    }
}
