/* @formatter:off
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
@formatter:on */

package seminar1.task2;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class minimumNumber {

    public static void main(String args[]) {

        System.out.println("Insert as many numbers as desired, " + "type 0 when you want to stop.\n"
                + "This program will find the minimum number.");
        Scanner scanner = new Scanner(System.in);
        List<Integer> numbers = new ArrayList<>();
        while (true) {
            System.out.print("\nWrite a number: ");
            if (scanner.hasNextInt()) {
                Integer number = scanner.nextInt();
                if (number == 0)
                    break;
                numbers.add(number);
            } else {
                System.out.println("Wrong input");
                scanner.next();
            }
        }
        scanner.close();
        Integer min = Collections.min(numbers);
        System.out.println("\nThe minimum number that you have entered was: " + min);
    }

}
