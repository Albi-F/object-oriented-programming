package seminar2;

import java.util.ArrayList;
import java.util.Optional;

import seminar2.Account.AccountType;

public class Customer {

    private String firstName;
    private String lastName;
    private static int IDcounter = 0;
    private String customerID;
    private ArrayList<Account> accounts = new ArrayList<>();

    public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.customerID = String.format("%03d", ++IDcounter);
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public String getID() {
        return this.customerID;
    }

    public ArrayList<Account> getAllBankAccounts() {
        return this.accounts;
    }

    public Optional<Account> getAccount(String accountNumber) {
        for (Account account : this.accounts) {
            if (account.getAccountNumber().equals(accountNumber)) { return Optional.of(account); }
        }
        return Optional.empty();
    }

    public Optional<Account> createAccount(AccountType type, int initialBalance) {
        Account newAccount = new Account(type, initialBalance);
        if (accounts.add(newAccount)) {
            return Optional.of(newAccount);
        }
        return Optional.empty();
    }

    public boolean closeAccount(String accountNumber) {
        for (Account account : this.accounts) {
            if (account.getAccountNumber().equals(accountNumber)) { return closeAccount(account); }
        }
        return false;
    }

    public boolean closeAccount(Account account) {
        return this.accounts.remove(account);
    }

    public String allAccountsDetailsToString() {
        StringBuilder accountsDetails = new StringBuilder();
        for (Account account : this.accounts) {
            accountsDetails.append(account.toString() + "\n");
        }
        if (accountsDetails.length() > 0) {
            accountsDetails.setLength(accountsDetails.length() -1);
            return accountsDetails.toString();
        }
        return "This customer doesn't have any account";
        
    }

    public boolean isAccountNumberValid(String number) {
        for (Account account : this.accounts) {
            if (account.getAccountNumber().equals(number)) { return true; }
        }
        return false;
    }

    @Override
    public String toString() {
        return "The customer name is: " + this.firstName + " " + this.lastName + ", his/her ID is: " + this.customerID;
    }
}
