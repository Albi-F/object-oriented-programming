package seminar2;

import java.util.Optional;

/*
Syncronized is used for thread safety,
this way, at any time, only one thread can access the method.
*/

public class Account {

    // There are 2 types of accounts
    public enum AccountType {
        // (credit limit, interest rate)
        CREDIT(5000, 0.5f),
        SAVING(0, 1f);

        private final int creditLimit;
        private final float interestRate;

        // Constructor mandatory when enums have instance variables
        private AccountType(int minBalance, float interestRate) {
            this.creditLimit = minBalance;
            this.interestRate = interestRate;
        }

        // When printing the type of the account I want it lowercase
        @Override
        public String toString() {
        return name().toLowerCase();
        }
    }
    
    // Every account has a different number
    // Accounts are numbered starting from 1
    private static int accountsNumber;

    // Instance variables
    private String number;
    private AccountType type;
    protected int balance;

    // Constructor
    public Account(AccountType type, int initialBalance) {
        this.number = String.format("%05d", ++accountsNumber);
        this.type = type;
        this.balance = initialBalance;
    }
    
    // ----- Account number -----
    public String getAccountNumber() {
        return this.number;
    }
    // --------------------


    // ----- Account type -----
    public AccountType getAccountType() {
        return this.type;
    }
    // --------------------


    // ----- Balance -----
    // Deposit is always possible if the amount id >= 0,
    // there is no upper limit
    public synchronized Optional<Integer> deposit(int amount) {
        if (amount > 0) { return Optional.of(this.balance += amount); }
        else { return Optional.empty(); }
    }

    // Withdraw can be not possible
    // depending on the money currently available
    public synchronized Optional<Integer> withdraw(int amount) {
        if (amount <= 0) { return Optional.empty(); }
        int newBalance = balance - amount;
        if (newBalance >= -this.type.creditLimit) {
            this.balance = newBalance;
            return Optional.of(this.balance);
        }
        else { return Optional.empty(); }
    }
    // --------------------


    public int getInterest() {
        return (int)(this.balance * (this.type.interestRate / 100));
    }

    @Override
    public String toString() {
        return this.type + " account with number: " + this.number + ", balance: "
               + this.balance + "SEK, interest rate: " + this.type.interestRate + "%";
    }

}
