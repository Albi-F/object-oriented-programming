package seminar2;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;

import seminar2.Account.AccountType;

public class BankLogic {

    static Optional<Customer> user = Optional.empty();
    
    // Menu to be printed
    static String menu = """
    1. Create a saving account for the customer
    2. Create a credit account for the customer
    3. View all accounts
    4. Deposit money
    5. Withdraw money
    6. Close account
    7. Exit
    What do you want to do?\s""";
    
    // Ask customer ID
    public static void logic(ArrayList<Customer> customers) {

        String requestedID;

        Scanner scanner = new Scanner(System.in);

        while (!user.isPresent()) {
            System.out.print("\nEnter your ID: ");
            requestedID = scanner.next();
            for (Customer customer : customers) {
                if (requestedID.equals(customer.getID())) {
                    user = Optional.of(customer);
                    break;
                }
            }
            if (!user.isPresent()) {
                System.out.println("Customer not found");
            }
        }

        System.out.println(user.get().getFullName());

        menu_loop: while(true) {
            System.out.print("\n" + menu);
            String choice = scanner.next();
            switch(choice) {
                case "1":
                    createAccount(AccountType.SAVING);
                    break;
                case "2":
                    createAccount(AccountType.CREDIT);
                    break;
                case "3":
                    viewAllAccounts();
                    break;
                case "4":
                    depositMoney();
                    break;
                case "5":
                    withdrawMoney();
                    break;
                case "6":
                    closeAccount();
                    break;
                case "7":
                    break menu_loop;
                default:
                    System.out.println("Invalid choice");
            }
        }

        scanner.close();
    }

    // Menu Option 1 & 2
    private static void createAccount(AccountType type) {
        System.out.println("\nCreating new account...");
        int amount = 0;
        if (type != AccountType.CREDIT) { amount = askMoneyQuantity(); }
        String msg = "Error";
        Optional<Account> newAccount = user.get().createAccount(type, amount);
        if (newAccount.isPresent()) { msg = newAccount.get().toString(); }
        System.out.println(msg);
    }

    // Menu Option 3
    public static void viewAllAccounts(Customer customer) {
        System.out.println("\nBank accounts:");
        System.out.println(customer.allAccountsDetailsToString());
    }

    private static void viewAllAccounts() {
        viewAllAccounts(user.get());
    }

    // Menu Option 4
    private static void depositMoney() {
        System.out.println("\nDepositing money...");
        Optional<String> accountNumber = askAccountNumber();
        if (accountNumber.isEmpty()) { return; }
        int quantity = askMoneyQuantity();
        Optional<Integer> newBalance = user.get().getAccount(accountNumber.get()).get().deposit(quantity);
        String msg = newBalance.isPresent() ? "New balance: " + newBalance.get() : "Error";
        System.out.println(msg);
    }

    // Menu Option 5
    private static void withdrawMoney() {
        System.out.println("\nWithdrawing money...");
        Optional<String> accountNumber = askAccountNumber();
        if (accountNumber.isEmpty()) { return; }
        int quantity = askMoneyQuantity();
        Optional<Integer> newBalance= user.get().getAccount(accountNumber.get()).get().withdraw(quantity);
        String msg = newBalance.isPresent() ? "New balance: " + newBalance.get() : "Not enough money";
        System.out.println(msg);
    }

    // Menu Option 6
    private static void closeAccount() {
        System.out.println("\nClosing account...");
        Optional<String> accountNumber = askAccountNumber();
        if (accountNumber.isEmpty()) { return; }
        Optional<Account> account = user.get().getAccount(accountNumber.get());
        if (account.isPresent()) {
            int profit = account.get().getInterest();
            if (user.get().closeAccount(account.get())) {
                System.out.println("You interests amount to " + profit + "SEK");
            }
        }
        else { System.out.println("Something went wrong"); }
    }

    // ----- Input Methods -----
    private static int askMoneyQuantity() {
        System.out.println("How much money?");
        final Scanner scanner = new Scanner(new ForwardingInputStream(System.in));
        int amount = -1;
        try (scanner) {
            do {
            System.out.print("Please insert a value greater or equal to 0... ");
            if (scanner.hasNextInt()) amount = scanner.nextInt();
            else scanner.next();
            } while(amount < 0);
        }
        return amount;
    }

    private static Optional<String> askAccountNumber() {
        if (user.get().getAllBankAccounts().isEmpty()) {
            System.out.println("The customer doesn't have any account");
            return Optional.empty();
        }
        final Scanner scanner = new Scanner(new ForwardingInputStream(System.in));
        String accountNumber;
        try (scanner) {
            do {
                System.out.println("Please insert the account number... ");
                accountNumber = scanner.next();
                if (user.get().isAccountNumberValid(accountNumber)) break;
                System.out.println("Invalid!");
            } while (true);
        }
        return Optional.of(accountNumber);
    }
    // --------------------
}
