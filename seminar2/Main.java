/*
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
 */

package seminar2;

import java.util.ArrayList;
import java.util.Arrays;

import seminar2.Account.AccountType;

public class Main {

    // ArrayList of all the customers
    static final ArrayList<Customer> customers = new ArrayList<>();

    // Customers
    static final Customer customer1 = new Customer("Johnny", "B. Goode");
    static final Customer customer2 = new Customer("Angelina", "Jolie");
    static final Customer customer3 = new Customer("Pamela", "Lavata");
    
    public static void main(String[] args) {

        // Customer 1
        customer1.createAccount(AccountType.CREDIT, 12000);
        customer1.createAccount(AccountType.CREDIT, 1000);
        customer1.createAccount(AccountType.SAVING, 100000);

        // Customer 2
        customer2.createAccount(AccountType.SAVING, 23400);
        customer2.createAccount(AccountType.CREDIT, -1200);

        // Add customer to the customers ArrayList
        customers.addAll(Arrays.asList(customer1, customer2, customer3));

        // Print data about all hardcoded customers
        System.out.println("Hardcoded customers:\n");
        for (Customer customer : customers) {
            System.out.print(customer);
            BankLogic.viewAllAccounts(customer);
            System.out.println();
        }
        System.out.println("----------------------------------");

        BankLogic.logic(customers);

    }

}
