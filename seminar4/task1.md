```
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
```

```plantuml
@startuml component

left to right direction
skinparam packageStyle rectangle

actor customer

package professionals {
    actor cashier
    actor baker
}

package cafe {
   (buy cookies)
   (buy drinks)
   (make a reservation)
   (collect the reservation)
   (create business plans)
   (baking cookies)
   (planning)
   (purchasing goods)
}

(buy cookies) -- cashier
customer -- (buy cookies)
(buy drinks) ..> (buy cookies) : extends
customer -- (buy drinks)

customer -- (make a reservation)
customer -- (collect the reservation)
(collect the reservation) -- cashier
(collect the reservation) .> (buy cookies) : include

(create business plans) -- cashier
(create business plans) -- baker

(baking cookies) -- baker

(purchasing goods) .> (planning) : include
(purchasing goods) -- baker
(planning) -- baker

@enduml
```