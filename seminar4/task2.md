```
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
```

```plantuml
@startuml

skinparam classAttributeIconSize 0

class BankLogic {
    -customers: Customer[]
    -name: String

    «Create» BankLogic(name: String)
    +getCustomers(): Customer[]
    +getName(): String
}

BankLogic "1" o-- "0..*" Customer

class Customer {
    -ID: Integer 
    -name: String
    -bank accounts: Account[]

    «Create» Customer(name: String)
    -{static} createID(): Integer
    +getID(): Integer
    +getName(): String
    +getAccounts(): Account[]
    +addSavingAccount(): void
    +addCreditAccount(): void
}

Customer "1" *-- "1..*" Account

abstract class Account {

    -account number: Integer 
    ~account type: String
    -balance: Integer
    -interest rate: Double

    ~deposit(amount: Integer): void
    ~{abstract} withdraw(amount: Integer): void
    -{static} createAccountNumber(): Integer
    ~getAccountNumber(): Integer
    ~calculateInterest(): Integer
    +printAccountInfo(): void
}

class SavingAccount {

    «Create» SavingAccount()
    ~withdraw(amount: Integer): void
}

 Account <|-- SavingAccount

class CreditAccount {

    «Create» CreditAccount()
    ~credit limit: Integer

    ~withdraw(amount: Integer): void
}

Account <|-- CreditAccount

```