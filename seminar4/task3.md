```
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
```

* Create a saving account for the customer

```plantuml
@startuml

hide footbox

actor customer
participant ":BankLogic" as BankLogic
participant ":Customer" as Customer
participant ":SavingAccount" as SavingAccount

loop until check ID is successful
BankLogic -> customer: Request Customer ID
customer --> BankLogic: Insert Customer ID
BankLogic -> BankLogic: checkCustomer(userID)
end

BankLogic -> Customer: getCustomer(userID)
customer -> BankLogic: Open Saving Account
BankLogic -> Customer: createSavingAccount()
create SavingAccount
Customer -> SavingAccount: <<create>>

@enduml
```

---

* Withdraw money from credit account

```plantuml
@startuml

actor customer
participant ":BankLogic" as BankLogic
participant ":Customer" as Customer
participant ":CreditAccount" as CreditAccount

loop until customer ID is valid
BankLogic -> customer: Request Customer ID
customer --> BankLogic: Insert Customer ID
BankLogic -> BankLogic: checkCustomer(userID)
end

BankLogic -> Customer: getCustomer(userID)
customer -> BankLogic: Request money withdrawal from credit account

loop until account ID is valid
BankLogic -> customer: Request Account ID
customer --> BankLogic: Insert Account ID
BankLogic -> Customer: checkAccount(accountID)
end

Customer -> CreditAccount: getAccount(accountID)

loop until amount is allowed
BankLogic -> customer: Request Amount
customer --> BankLogic: Insert Amount
BankLogic -> CreditAccount: checkAmount(amount)
end

customer -> CreditAccount: Withdraw Money

@enduml
```

---

* View all accounts for a customer

```plantuml

@startuml

hide footbox

actor customer
participant ":BankLogic" as BankLogic
participant ":Customer" as Customer

loop until customer ID is valid
BankLogic -> customer: Request Customer ID
customer --> BankLogic: Insert Customer ID
BankLogic -> BankLogic: checkCustomer(userID)
end

BankLogic -> Customer: getCustomer(userID)
customer -> BankLogic: Request Account List
BankLogic -> Customer: getAccountsList()
Customer --> customer: Print Accounts List
@enduml
```