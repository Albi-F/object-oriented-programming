package seminar3.controller;

import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import seminar3.model.DataStorage;

public class RegisterController implements Initializable {

    @FXML
    private TextField emailAddress;

    @FXML
    private Button registrationButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        registrationButton.setDisable(true);
        emailAddress.textProperty().addListener(new ChangeListener<>(){
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                // System.out.println("Observable: " + observable  + ", oldValue: " + oldValue + ", newValue: " + newValue);
                if (validate(newValue)) {
                    registrationButton.setDisable(false);
                }
                else {
                    registrationButton.setDisable(true);
                }
            }
        });
    }

    @FXML
    public void handleRegisterButtonAction(ActionEvent event) throws IOException {

        DataStorage storage = DataStorage.getInstance();
        storage.addEmail(emailAddress.getText());

        goToDisplayScene(event);
    }

    public void handleGoToListButtonAction(ActionEvent event) throws IOException {
        goToDisplayScene(event);
    }

    public void goToDisplayScene(ActionEvent event) throws IOException {
        Node node = (Node)event.getSource();
        Stage stage = (Stage)node.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/display.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
}
    
}
