package seminar3.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import seminar3.model.DataStorage;

public class DisplayController  implements Initializable {

    @FXML
    ListView<String> emailList;
    DataStorage storage;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        storage = DataStorage.getInstance();
        emailList.setItems(storage.getEmails());
    }

    public void handleGoToRegistrationButtonAction(ActionEvent event) throws IOException {
        goToRegistrationScene(event);
    }

    public void goToRegistrationScene(ActionEvent event) throws IOException {
        Node node = (Node)event.getSource();
        Stage stage = (Stage)node.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/registration.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
    }

    public void handleRemoveSelectedEmail(ActionEvent event) throws IOException {
        int index = emailList.getSelectionModel().getSelectedIndex();
        if (index != -1) {
            storage.removeEmail(index);
        }
    }
    
}
