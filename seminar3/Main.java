/* @formatter:off
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
@formatter:on */

package seminar3;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        ViewSwitcher viewSwitcher = new ViewSwitcher();
        viewSwitcher.switchView("view/registration.fxml", primaryStage, "Database");
    }

    public static void main(String[] args) {
        launch(args);
    }
}