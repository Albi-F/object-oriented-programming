package seminar3.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DataStorage {
    private static DataStorage myStorage;
    private ObservableList<String> emails;

    private DataStorage() {
        emails = FXCollections.observableArrayList();
    }

    public static DataStorage getInstance() {
        if (myStorage == null) {
            myStorage = new DataStorage();
        }

        return myStorage;
    }

    public ObservableList<String> getEmails() {
        return this.emails;
    }

    public void addEmail(String email) {
        this.emails.add(email);
    }

    public void removeEmail(int index) {
        this.emails.remove(index);
    }
}
