/* @formatter:off
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
@formatter:on */

package tests.test1;

import java.util.Optional;

public class test1 {

    public static void main(String args[]) {
        // an int can't be null but Integer is fine
        Integer nullInt1 = null;
        System.out.println(nullInt1);

        optionalEmpty();
        optionalString();
    }

    public static void optionalEmpty() {
        Optional<Object> empty = Optional.empty();
        System.out.println("\nVariable: " + empty);
        System.out.println(empty.isPresent());
        System.out.println(empty.isEmpty());
    }

    public static void optionalString() {
        Optional<String> hello = Optional.of("Hello");
        System.out.println("\nVariable: " + hello);
        System.out.println(hello.isPresent());
        System.out.println(hello.isEmpty());
        String orElse = hello.orElse("World");
        System.out.println(orElse);
    }
}
