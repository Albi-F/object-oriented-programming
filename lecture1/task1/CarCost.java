/* @formatter:off
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
@formatter:on */

package lecture1.task1;

import java.util.Scanner;

public class CarCost {

    static double CarCostPerDay = 124.99;

    public static void main(String arg[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("How many days do you want to rent it? ");
        int numberOfDays = scanner.nextInt();
        scanner.close();
        double totalCost = CarCostPerDay * numberOfDays;
        System.out.println("The price is: " + totalCost);
    }
}
