/* @formatter:off
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
@formatter:on */

package lecture1.task5;

import java.util.Scanner;

public class IntegerSum {

    public static void main(String args[]) {
        System.out.print("Write a number... ");
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();
        scanner.close();
        long sum = 0;
        for (; value > 0; value--) {
            sum += value;
        }
        System.out.println("The sum is: " + sum);
    }

}
