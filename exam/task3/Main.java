package exam.task3;

import java.util.ArrayList;

public class Main {

    public static void main(String args[]) {

        Book book1 = new Book("A new World", "Alex", "Wonderful Things", "22/05/2018", 20);
        Book book2 = new Book("Java Book", "Maria Rossi", "The Future", "04/04/2010", 25);

        ArrayList<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        for (Book book : books) {
            book.printDetails();
        }
    }
}
