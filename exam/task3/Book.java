package exam.task3;

public class Book {

    private String title;
    private String author;
    private String publisher;
    private String publicationDate;
    private int price;
    private ISBN isbn = new ISBN();

    public Book(String title, String author, String publisher, String publicationDate, int price) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.publicationDate = publicationDate;
        this.price = price;
    }

    public void setBookISBN(int language, int publisher, int title, int check) {
        isbn.setISBN(language, publisher, title, check);
    }

    public String getBookISBN() {
        return isbn.getISBN();
    }

    public void printDetails() {

        System.out.println("Book Title: " + this.title);
        System.out.println("Author: " + this.author);
        System.out.println("Publisher: " + this.publisher);
        System.out.println("ISBN: " + this.getBookISBN());
    }
    
}
