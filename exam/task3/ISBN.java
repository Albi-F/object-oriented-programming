package exam.task3;

public class ISBN {

    private int language;
    private int publisher;
    private int title;
    private int check;

    public ISBN() {}

    public ISBN(int language, int publisher, int title, int check) {
        this.setISBN(language, publisher, title, check);
    }

    public String getISBN() {
        return Integer.toString(this.language) + Integer.toString(this.publisher) + Integer.toString(this.title) + Integer.toString(this.check);
    }

    public void setISBN(int language, int publisher, int title, int check) {
        // language
        if (language < 0) {
            language = -language;
        }
        this.language = language;

        // publisher
        if (publisher < 0) {
            publisher = -publisher;
        }
        this.publisher = publisher;

        // title
        if (title < 0) {
            title = -title;
        }
        this.title = title;

        // check
        if (check < 0) {
            check = -check;
        }
        this.check = check;
    }
}
