package exam.task2;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String args[]) {

        int[] numbers = {3, 29, 20, 7, 18};
        int[] result = divisibleBy3or5(numbers);
        System.out.println(Arrays.toString(result));

    }

    public static int[] divisibleBy3or5(int[] numbers) {
        ArrayList<Integer> validNumbers = new ArrayList<>();
        for (int number: numbers) {
            if (number % 3 == 0 || number % 5 == 0) {
                validNumbers.add(number);
            }
        }
        return validNumbers.stream().mapToInt(i -> i).toArray();
    }
    
}
