package exam.task8;

public class Message {

    private String text;

    public Message(String text) {
        this.setMessageText(text);
    }
    
    public String getMessageText() {
        return text;
    }

    public void setMessageText(String text) {
        this.text = text;
    }

}
