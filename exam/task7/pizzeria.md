```plantuml
@startuml

skinparam classAttributeIconSize 0

class Pizzeria {
    -pizzas: Pizza[]
    -toppings: Topping[]

    +Pizzeria()
    +getPizzas(): Pizza[]
    +addPizza(pizza: Pizza): void
    +getToppings(): Topping[]
}

Pizzeria "1" o-- "1..*" Pizza
Pizzeria "1" o-- "1..*" Topping

class Pizza {
    -name: String
    -toppings: Topping[]

    +Pizza(name: String, toppings: Topping[])
    +getName(): String
    +getToppings(): Topping[]
    +addTopping(topping: Topping): void
    +removeTopping(topping: Topping): void
}

Pizza "1" o-- "1..*" Topping

abstract class Topping {
    -name: String

    +getName(): String
}

class NonExpirableTopping {

    +NonExpirableTopping(name: String)
}

 Topping <|-- NonExpirableTopping

class ExpirableTopping {
    -expiry date: String

    +ExpirableTopping(name: String, expiry date: String)
    +getExpiryDate(): String
}

Topping <|-- ExpirableTopping

```